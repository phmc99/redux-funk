import "./App.css";

import { useDispatch, useSelector } from "react-redux";

import { addCommentThunk } from "./store/modules/user/thunks";
import { useState } from "react";

function App() {
  const [comentario, setComentario] = useState("");

  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const handleClick = () => dispatch(addCommentThunk(comentario));

  return (
    <div className="App">
      <header className="App-header">
        <input
          value={comentario}
          type="text"
          onChange={(e) => setComentario(e.target.value)}
        />
        <button onClick={handleClick}>Adicionar</button>
        <div>{user.comments}</div>
      </header>
    </div>
  );
}

export default App;

import { ADD_COMMENT } from "./actionsType";

export const addComment = (updatedUser, comments) => ({
  type: ADD_COMMENT,
  updatedUser: updatedUser,
  comments: comments,
});
